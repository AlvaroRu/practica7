package mx.unitec.moviles.practica7.repository

import android.app.Application
import androidx.lifecycle.LiveData
import mx.unitec.moviles.practica7.dao.ContactDao
import mx.unitec.moviles.practica7.db.ContactsDatabase
import mx.unitec.moviles.practica7.model.Contact

class ContactsRepository(application: Application) {
    private val contactDao: ContactDao? = ContactsDatabase.getInstance(application)?.contactDao()

    suspend fun insert(contact: Contact) {
        contactDao!!.insert(contact)
    }
    suspend fun delete(contact: Contact) {
        contactDao!!.delete(contact)
    }

    fun getContacts(): LiveData<List<Contact>> {
        return contactDao!!.getOrderedAgenda()
    }
}